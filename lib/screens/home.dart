import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my_app/podo/Categorie.dart';
import 'package:my_app/podo/CategorieLab.dart';
import 'package:my_app/podo/Restaurant.dart';
import 'package:my_app/podo/RestaurantLab.dart';
import 'package:my_app/podo/User.dart';
import 'package:my_app/screens/dishes.dart';
import 'package:my_app/widgets/grid_product.dart';
import 'package:my_app/widgets/home_category.dart';
import 'package:my_app/widgets/slider_item.dart';
import 'package:my_app/util/foods.dart';
import 'package:my_app/util/categories.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with AutomaticKeepAliveClientMixin<Home>{
  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  int _current = 0;
  List<Categorie> ca = CategorieLab.get().categories;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(

      body: Padding(
        padding: EdgeInsets.fromLTRB(10.0,0,10.0,0),
        child: ListView(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Dishes",
                  style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w800,
                  ),
                ),

                FlatButton(
                  child: Text(
                    "View More",
                    style: TextStyle(
//                      fontSize: 22,
//                      fontWeight: FontWeight.w800,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context){
                          return DishesScreen();
                        },
                      ),
                    );
                  },
                ),
              ],
            ),

            SizedBox(height: 10.0),

            //Slider Here

            CarouselSlider(
              height: MediaQuery.of(context).size.height/2.4,
              items: map<Widget>(
                foods,
                    (index, i){
                      Map food = foods[index];
                  return SliderItem(
                    img: food['img'],
                    isFav: false,
                    name: food['name'],
                    rating: 5.0,
                    raters: 23,
                  );
                },
              ).toList(),
              autoPlay: true,
//                enlargeCenterPage: true,
              viewportFraction: 1.0,
//              aspectRatio: 2.0,
              onPageChanged: (index) {
                setState(() {
                  _current = index;
                });
              },
            ),
            SizedBox(height: 20.0),

            Text(
              "Food Categories",
              style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.w800,
              ),
            ),
            SizedBox(height: 10.0),

            Container(
              height: 65.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,

              itemCount: ca == null?0:ca.length,
                itemBuilder: (BuildContext context, int index) {
                  Categorie cat = ca[index];

                  return HomeCategory(
                    icon:null ,//cat['icon']
                    title: cat.nom,
                    items:"05" ,//cat['items'].toString()
                    isHome: true,
                  );
                },
              ),
            ),

            SizedBox(height: 20.0),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Popular Items",
                  style: TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w800,
                  ),
                ),

                FlatButton(
                  child: Text(
                    "View More",
                    style: TextStyle(
//                      fontSize: 22,
//                      fontWeight: FontWeight.w800,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  onPressed: (){},
                ),
              ],
            ),
            SizedBox(height: 10.0),

            GridView.builder(
              shrinkWrap: true,
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: MediaQuery.of(context).size.width /
                    (MediaQuery.of(context).size.height / 1.25),
              ),
              itemCount: foods == null ? 0 :foods.length,
              itemBuilder: (BuildContext context, int index) {
//                Food food = Food.fromJson(foods[index]);
                Map food = foods[index];
//                print(foods);
//                print(foods.length);
                return GridProduct(
                  img: food['img'],
                  isFav: false,
                  name: food['name'],
                  rating: 5.0,
                  raters: 23,
                );
              },
            ),

            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }


  @override
  bool get wantKeepAlive => true;
////////////
  /*
  fetchRestaurants(nomcategorie,nomuser) async {
    String message = "";
    var url = "http://192.168.1.52:4000/getAllRestwithCat/"; // iOS
    try
    {

      final http.Response response = await http.post(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode (<String, String>{
          'value':nomcategorie,
          'userfavoris':nomuser,

        }),
      );
      if (response.statusCode == 200) {
        // If the call to the server was successful, parse the JSON to get user data
       /* print("************************");
        print("response"+response.body);
        print("**************************");*/
        RestaurantLab.fromJson(json.decode(response.body));
          List<Restaurant> ca = RestaurantLab.get().restaurants;
         print(ca[0].nom);
      } else {
        message = 'Cannot fetch Restaurants from servers';
      }
    } catch (e) {
      message = 'Cannot fetch Restaurants from servers';
    }
    if (message != "") {
      final snackBar =
      SnackBar(content: Text(message), duration: Duration(seconds: 2));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }*/

}
